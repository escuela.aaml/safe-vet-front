import './App.css';
import Navbarr from './components/Navbarr';
import Appointment from './screens/Appointment';
import Customers from './screens/Customers';
import MedicalRecords from './screens/MedicalRecords';
import Login from './screens/Login';
import 'semantic-ui-css/semantic.min.css'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink 
} from "react-router-dom";
import Register from './screens/Register';


function App() {
  return (
    <div >
      <Router>
        <div>
            <Switch>
            <Route path="/" exact>
                    <Login></Login>
                </Route>
                <Route path="/registro" exact>
                    <Register></Register>
                </Route>
                <Route path="/citas" exact>
                    <Appointment></Appointment>
                </Route>
                <Route path="/clientes" exact>
                    <Customers></Customers>
                </Route>
                <Route path="/expedientes" exact>
                    <MedicalRecords></MedicalRecords>
                </Route>
                <Route path="/cuenta" exact>
                    cuenta...
                </Route>

            </Switch>
        </div>
        </Router>
    </div>
  );
}

export default App;
