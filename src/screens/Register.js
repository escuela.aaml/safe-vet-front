import React from 'react'
import "../components/MyStyles.css";
import {Container, Row, Col } from "react-bootstrap";
import { Input,Button } from 'semantic-ui-react'
import { useHistory} from 'react-router-dom';
const Register = () => {
    const history = useHistory();





    function validateForm(){
        history.push("/citas")
    }
    return (
        <div style={{minHeight: '100vh',width: '100%',backgroundColor:"#0791FB"}}>
        <Container
         className="flexMain" style={{minHeight: '100vh',width: '100%'}}
        >
          <Row  className="loginRow">
            <Col xs={12}><h1 style={{ textAlign:"center",marginTop:20,color:"black"}}>SAFE VET</h1></Col>
            <Col xs={12}> <label style={{ marginTop:5,fontSize:15,fontWeight:"bold"}}>Usuario</label></Col>
            <Col xs={12}><Input style={{width:"100%"}} placeholder='Usuario' /></Col>
            <Col xs={12}> <label style={{ marginTop:10,fontSize:15,fontWeight:"bold"}}>Nombre</label></Col>
            <Col xs={12}><Input style={{width:"100%"}} placeholder='Nombre' /></Col>
            <Col xs={12}> <label style={{ marginTop:10,fontSize:15,fontWeight:"bold"}}>Apellido Paterno</label></Col>
            <Col xs={12}><Input style={{width:"100%"}} placeholder='Apellido Paterno' /></Col>
            <Col xs={12}> <label style={{ marginTop:10,fontSize:15,fontWeight:"bold"}}>Apellido Materno</label></Col>
            <Col xs={12}><Input style={{width:"100%"}} placeholder='Apellido Materno' /></Col>
            <Col xs={12}> <label style={{ marginTop:10,fontSize:15,fontWeight:"bold"}}>Telefono</label></Col>
            <Col xs={12}><Input style={{width:"100%"}} placeholder='Telefono' /></Col>
            <Col xs={12}> <label style={{ marginTop:10,fontSize:15,fontWeight:"bold"}}>Domicilio</label></Col>
            <Col xs={12}><Input style={{width:"100%"}} placeholder='Domicilio' /></Col>
            <Col xs={12}> <label style={{ marginTop:10,fontSize:15,fontWeight:"bold"}}>Sexo</label></Col>
            <Col xs={12}><Input style={{width:"100%"}} placeholder='Sexo' /></Col>
            <Col xs={12}> <label style={{ marginTop:10,fontSize:15,fontWeight:"bold"}}>Fecha de nacimiento</label></Col>
            <Col xs={12}><Input style={{width:"100%"}} placeholder='Fecha de nacimiento' /></Col>
            <Col xs={12}> <label style={{ marginTop:10,fontSize:15,fontWeight:"bold"}}>Correo</label></Col>
            <Col xs={12}><Input style={{width:"100%"}} placeholder='Correo' /></Col>
            <Col xs={12}> <label style={{ marginTop:10,fontSize:15,fontWeight:"bold"}}>Contraseña</label></Col>
            <Col xs={12}><Input style={{width:"100%"}} placeholder='Contraseña' /></Col>
            <Col style={{display: 'flex',justifyContent: 'center'}} xs={12}> 
            <Button style={{width:"60%",marginTop:20,marginBottom:10}} secondary onClick={() => validateForm()}>
                Registrarse 
            </Button>
            </Col>
            <Col xs={12}><label style={{fontSize:15,fontWeight:"bold"}}>¿ya estás registrado? <a style={{color:"black"}} href="/"><b>¡Inicia sesion!</b></a></label></Col>
          </Row>
        </Container>
      </div>
    )
}

export default Register
