import React from "react";
import "../components/MyStyles.css";
import { Nav, Container, Row, Col } from "react-bootstrap";
import { Input,Button } from 'semantic-ui-react'

const Login = () => {
  return (
    <div style={{minHeight: '100vh',width: '100%',backgroundColor:"#0791FB"}}>
      <Container
       className="flexMain" style={{minHeight: '100vh',width: '100%'}}
      >
        <Row  className="loginRow">
          <Col xs={12}><h1 style={{ textAlign:"center",marginTop:20,color:"black"}}>SAFE VET</h1></Col>
          <Col xs={12}><h3 style={{ marginTop:30}}>Inicio de sesión</h3></Col>
          <Col xs={12}><h6 style={{ marginTop:5}}>Por favor, inicie sesión para continuar</h6></Col>
          <Col xs={12}><Input style={{width:"100%",marginTop:10,}} placeholder='Usuario' /></Col>
          <Col xs={12}><Input style={{width:"100%",marginTop:10}} placeholder='Contraseña' /></Col>
          <Col style={{display: 'flex',justifyContent: 'center'}} xs={12}> <Button style={{width:"60%",marginTop:30}} secondary>Inicio Sesión </Button></Col>
          <Col xs={12}><h6 style={{marginTop:10}}>¿Olvidaste tu contraseña?</h6></Col>
          <Col xs={12}><h6 style={{marginTop:5,marginBottom:20}}>¿No estás registrado? <a style={{color:"black"}} href="/registro"><b>¡Registrate!</b></a></h6></Col>
        </Row>
      </Container>
    </div>
  );
};

export default Login;
